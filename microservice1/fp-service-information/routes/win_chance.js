const express = require('express');
const router = express.Router();
const faker = require('faker');

/* GET home page. */
router.get('/', (req, res) => {
   var events = [ 
      { "first_team": faker.commerce.productName(), "second_team": faker.commerce.productName(), "sport": faker.random.arrayElement(['Football', 'Tennis', 'Volleyall', 'Basketball'])},
      { "first_team": faker.commerce.productName(), "second_team": faker.commerce.productName(), "sport": faker.random.arrayElement(['Football', 'Tennis', 'Volleyall', 'Basketball'])},
      { "first_team": faker.commerce.productName(), "second_team": faker.commerce.productName(), "sport": faker.random.arrayElement(['Football', 'Tennis', 'Volleyall', 'Basketball'])},
      { "first_team": faker.commerce.productName(), "second_team": faker.commerce.productName(), "sport": faker.random.arrayElement(['Football', 'Tennis', 'Volleyall', 'Basketball'])},
      { "first_team": faker.commerce.productName(), "second_team": faker.commerce.productName(), "sport": faker.random.arrayElement(['Football', 'Tennis', 'Volleyall', 'Basketball'])},
      { "first_team": faker.commerce.productName(), "second_team": faker.commerce.productName(), "sport": faker.random.arrayElement(['Football', 'Tennis', 'Volleyall', 'Basketball'])},
  	];
    res.render('win_chance', 
  			{ "events": events }
  	);
});
module.exports = router;
