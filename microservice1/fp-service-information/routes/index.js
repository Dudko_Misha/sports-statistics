const express = require('express');
const router = express.Router();
const reqlib = require('app-root-path').require;
const logger = reqlib('logger');
const os = require("os");

const util = require('util')
const chance = require('chance')
const faker = require('faker')

const packageGenVersion = reqlib('lib/version.js') 

/* GET home page. */
router.get('/', (req, res) => {
	res.render('index', 
	  	{ team:  'StatExplprers', 
	  	  year: 2020,
	  	  version: packageGenVersion,
	  	});

  
});

module.exports = router;
