const express = require('express');
const router = express.Router();

const jsf = require('json-schema-faker');
const util = require('util')
const chance = require('chance')
const faker = require('faker')
jsf.extend('chance', () => new chance.Chance());
jsf.extend('faker', () => faker);

var schema = {
  "type": "array",
  "minItems": 10,
  "maxItems": 20,
  "items": {
	  "type": "object",
	  "properties": {
	    "name": {
	      "type": "string",
	      "faker": "name.findName"
	    },
	    "cost" : {
	      "type": "integer", 
	       "minimum": 500,
  		   "maximum": 10000
	    },
	    "age" : {
	    	"type": "string", 
	    	"chance": {
	    		 "age": {
	      			"type": "adult"
	    		 }
	    	}
	    },
	     "sport_name" : {
	    	"type": "array",
  			"minItems": 1,
  			"maxItems": 1,
  			"items": {
    			"enum": [
      				"Football",
      				"Basketball",
      				"Volleyball",
      				"Tennis"
   				]
  			}
	    },
	     "day" : {
	      "type": "integer", 
	       "minimum": 1,
  		   "maximum": 28
	    },
	     "month" : {
	      "type": "integer", 
	       "minimum": 1,
  		   "maximum": 12
	    },
	  },
	  "required": [
	    "name",
	    "age", 
	    "day",
	    "month",
	    "cost",
	    "sport_name"
	   ]
	  }
};

/* GET home page. */
router.get('/', (req, res) => {

  jsf.resolve(schema).then(sample => {
  	   console.log(util.inspect(sample, 
  	   	{showHidden: false, depth: null}));
	   
	   res.render('Information', 
	  	{  need_information:  sample });
  });

  
});

module.exports = router;
