<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xls="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h1>Sport statistics</h1>
                <xls:apply-templates select="sport/type"/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="type">
        <h2>Football matches:</h2>
        <xsl:apply-templates select="//football"/>
        <h2>Tennis matches:</h2>
        <xsl:apply-templates select="//tennis"/>
        <h2>Basketball matches:</h2>
        <xsl:apply-templates select="//basketball"/>
        <h2>Volleyball matches:</h2>
        <xsl:apply-templates select="//volleyball"/>
    </xsl:template>

    <xsl:template match="football">
        <div><b>Match ID:</b><xsl:value-of select="generate-id(.)"/></div>
        <table>
            <tr>
                <th>Team number</th>
                <th>Team name</th>
                <th>Goals</th>
                <th>Wins</th>
                <th>Coefficients</th>
            </tr>
            <tr><xsl:apply-templates select="team1"/></tr>
            <tr><xsl:apply-templates select="team2"/></tr>
        </table>
    </xsl:template>

    <xsl:template match="football/team1">
        <td>№1</td>
        <td><xsl:value-of select="@team_name"/></td>
        <td><xsl:value-of select="@Goals"/></td>
        <td><xsl:value-of select="@current_wins"/></td>
        <td><xsl:value-of select="@coefficient"/></td>
    </xsl:template>
    <xsl:template match="football/team2">
        <td>№2</td>
        <td><xsl:value-of select="@team_name"/></td>
        <td><xsl:value-of select="@Goals"/></td>
        <td><xsl:value-of select="@current_wins"/></td>
        <td><xsl:value-of select="@coefficient"/></td>
    </xsl:template>

    <xsl:template match="tennis">
        <div><b>Match ID:</b><xsl:value-of select="generate-id(.)"/></div>
        <table>
            <tr>
                <th>Player number</th>
                <th>Player name</th>
                <th>Sets</th>
                <th>Wins</th>
                <th>Coefficients</th>
            </tr>
            <tr><xsl:apply-templates select="player1"/></tr>
            <tr><xsl:apply-templates select="player2"/></tr>
        </table>
    </xsl:template>

    <xsl:template match="tennis/player1">
        <td>№1</td>
        <td><xsl:value-of select="@name"/></td>
        <td><xsl:value-of select="@sets"/></td>
        <td><xsl:value-of select="@current_wins"/></td>
        <td><xsl:value-of select="@coefficient"/></td>
    </xsl:template>
    <xsl:template match="tennis/player2">
        <td>№2</td>
        <td><xsl:value-of select="@name"/></td>
        <td><xsl:value-of select="@sets"/></td>
        <td><xsl:value-of select="@current_wins"/></td>
        <td><xsl:value-of select="@coefficient"/></td>
    </xsl:template>

    <xsl:template match="volleyball">
        <div><b>Match ID:</b><xsl:value-of select="generate-id(.)"/></div>
    <table>
        <tr>
            <th>Team number</th>
            <th>Team name</th>
            <th>Score</th>
            <th>Wins</th>
            <th>Coefficients</th>
        </tr>
        <tr><xsl:apply-templates select="team1"/></tr>
        <tr><xsl:apply-templates select="team2"/></tr>
    </table>
</xsl:template>

    <xsl:template match="volleyball/team1">
        <td>№1</td>
        <td><xsl:value-of select="@team_name"/></td>
        <td><xsl:value-of select="@Score"/></td>
        <td><xsl:value-of select="@current_wins"/></td>
        <td><xsl:value-of select="@coefficient"/></td>
    </xsl:template>
    <xsl:template match="volleyball/team2">
        <td>№2</td>
        <td><xsl:value-of select="@team_name"/></td>
        <td><xsl:value-of select="@Score"/></td>
        <td><xsl:value-of select="@current_wins"/></td>
        <td><xsl:value-of select="@coefficient"/></td>
    </xsl:template>

    <xsl:template match="basketball">
        <div><b>Match ID:</b><xsl:value-of select="generate-id(.)"/></div>
        <table>
            <tr>
                <th>Team number</th>
                <th>Team name</th>
                <th>Score</th>
                <th>Wins</th>
                <th>Coefficients</th>
            </tr>
            <tr><xsl:apply-templates select="team1"/></tr>
            <tr><xsl:apply-templates select="team2"/></tr>
        </table>
    </xsl:template>

    <xsl:template match="basketball/team1">
        <td>№1</td>
        <td><xsl:value-of select="@team_name"/></td>
        <td><xsl:value-of select="@Score"/></td>
        <td><xsl:value-of select="@current_wins"/></td>
        <td><xsl:value-of select="@coefficient"/></td>
    </xsl:template>
    <xsl:template match="basketball/team2">
        <td>№2</td>
        <td><xsl:value-of select="@team_name"/></td>
        <td><xsl:value-of select="@Score"/></td>
        <td><xsl:value-of select="@current_wins"/></td>
        <td><xsl:value-of select="@coefficient"/></td>
    </xsl:template>
</xsl:stylesheet>